package br.com.weverton.microservice.loja.loja.controller.dto;

public class InfoFornecedorDTO {
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    private String endereco;
}
