package br.com.weverton.microservice.loja.loja.repositoy;

import br.com.weverton.microservice.loja.loja.model.Compra;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompraRepository extends CrudRepository<Compra, Long> {
}
