package br.com.weverton.microservice.loja.loja;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;


@Configuration
public class ResorceServerConfigurer extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        /*para todas as requisições sejam autorizadas*/
//        http.authorizeRequests().anyRequest().authenticated();
        /* some as url informadas tem que ser autorizadas*/
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/compra").hasRole("USER");
    }
}
