package br.com.weverton.microservice.loja.loja.controller;

import br.com.weverton.microservice.loja.loja.controller.dto.CompraDTO;
import br.com.weverton.microservice.loja.loja.model.Compra;
import br.com.weverton.microservice.loja.loja.service.CompraService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("compra")
public class CompraController {
    private static final Logger LOG = LoggerFactory.getLogger(CompraController.class);

    @Autowired
    private CompraService compraService;

    @RequestMapping(method = RequestMethod.POST)
    public Compra realizaCompra(@RequestBody CompraDTO compraDTO) {
        LOG.info("realizar compra inicio =>");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return compraService.realizaCompraComFeign(compraDTO);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Compra getById(@PathVariable("id") Long id) {
        return compraService.getById(id);

    }
}
