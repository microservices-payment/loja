package br.com.weverton.microservice.loja.loja.controller.dto;

public class InfoPedidoDto {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTempoDePreparo() {
        return tempoDePreparo;
    }

    public void setTempoDePreparo(Integer tempoDePreparo) {
        this.tempoDePreparo = tempoDePreparo;
    }

    private Integer tempoDePreparo;

    @Override
    public String toString() {
        return "InfoPedidoDto{" +
                "id=" + id +
                ", tempoDePreparo=" + tempoDePreparo +
                '}';
    }
}
