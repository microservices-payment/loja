package br.com.weverton.microservice.loja.loja.service;

import br.com.weverton.microservice.loja.loja.client.FornecedorClient;
import br.com.weverton.microservice.loja.loja.controller.dto.CompraDTO;
import br.com.weverton.microservice.loja.loja.controller.dto.InfoFornecedorDTO;
import br.com.weverton.microservice.loja.loja.controller.dto.InfoPedidoDto;
import br.com.weverton.microservice.loja.loja.model.Compra;
import br.com.weverton.microservice.loja.loja.repositoy.CompraRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CompraService {
    private static final Logger LOG = LoggerFactory.getLogger(CompraService.class);
    @Autowired
    private RestTemplate client;
    /*exemplo de uso do Load balanced através do Rest Template*/
    @Autowired
    DiscoveryClient eurekaClient;

    @Autowired
    private FornecedorClient fornecedorClient;

    @Autowired
    private CompraRepository compraRepository;


    @HystrixCommand(fallbackMethod = "realizaCompraFallback")
    public Compra realizaCompraComFeign(CompraDTO compraDTO) {

        LOG.info("terá que buscar fornecedor pelo estado da compra =>");
        InfoFornecedorDTO info = this.fornecedorClient.getInfoPorEstado(compraDTO.getEndereco().getEstado());

        LOG.info("Retornou do fornecedor do estado " + compraDTO.getEndereco().getEstado() + "  de endereço => " + info.getEndereco());

        LOG.info("Realizar os pedidos da compra ");

        InfoPedidoDto infoPedidoDto = fornecedorClient.realizaPedido(compraDTO.getItens());

        LOG.info("Pedido realizado  => " + infoPedidoDto.toString());

        Compra compra = new Compra();

        compra.setEnderecoDeDestino(compraDTO.getEndereco().toString());
        compra.setPedidoId(infoPedidoDto.getId());
        compra.setTempoDePreparo(infoPedidoDto.getTempoDePreparo());


//        try {
//            LOG.info("thread funcionado = > > - > -> -> ");
//            Thread.sleep(7000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        try {
            Compra compra1 = compraRepository.save(compra);
            LOG.info("compra realizada =>" + compra1.toString());
            return compra1;
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }


    }


    public Compra realizaCompraFallback(CompraDTO compra) {
        Compra compraFallback = new Compra();
        compraFallback.setEnderecoDeDestino(compra.getEndereco().toString());
        return compraFallback;
    }


    public void realizaCompraComRestTemplate(CompraDTO compraDTO) {
        ResponseEntity<InfoFornecedorDTO> exchange = client.exchange("http://fornecedor/info/" + compraDTO.getEndereco().getEstado(), HttpMethod.GET, null, InfoFornecedorDTO.class);
        System.out.println(exchange.getBody().getEndereco());




        /* para fins de conhecimento*/
        eurekaClient.getInstances("fornecedor").stream()
                .forEach(fornecedor -> System.out.println("fornecedor rodando na porta" + fornecedor.getPort()));

    }

    @HystrixCommand
    public Compra getById(Long id) {
        return compraRepository.findById(id).orElse(new Compra());
    }
}
