package br.com.weverton.microservice.loja.loja;

import br.com.weverton.microservice.loja.loja.controller.CompraController;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableFeignClients
@EnableCircuitBreaker
@EnableEurekaClient
@EnableResourceServer
public class LojaApplication {

    private static final Logger LOG = LoggerFactory.getLogger(LojaApplication.class);

    @Bean
    public RequestInterceptor getInterceptorDeAutenticacao() {
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

                if (authentication == null) {
                    LOG.info("authentication  nullllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll");
                    return;
                }

                LOG.info("authentication ok =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails) authentication.getDetails();
                requestTemplate.header("Authorization", "Bearer" + oAuth2AuthenticationDetails.getTokenValue());
            }
        };
    }


    /*exemplo de uso do Load balanced através do Rest Template*/
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(LojaApplication.class, args);
    }

}
